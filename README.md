# Asteroid1-ECS-Unity-client-Prototype

unity client for https://gitlab.com/raojala/asteroid1-ecs-prototyp

## PROTOCOL BUFFERS TO UNITY!

- take your .proto file and generate c# files from it.
- create a new C# DLL .Net project on visual studio
- move protocol C# source file to DLL Project
- Install Nuget packages:
  - Google.Protobuf
  - Google.ProtocolBuffers
- build DLL
- Move DLL to Unity_Project_root/Assets/Plugins

