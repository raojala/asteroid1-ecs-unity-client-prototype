using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public static class SocketClient
{
    private static Thread networkThread = null;

    private static Socket client = null;
    public static Queue<byte[]> PacketQueue = new Queue<byte[]>();

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void StartSocketClient()
    {
        try
        {
            Debug.Log("Starting Socket client...");
            client = new Socket(SocketType.Stream, ProtocolType.Tcp);
            client.Connect("127.0.0.1", 10111);

            Application.quitting += ApplicationIsQuitting;
            StartNetworkThread();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    static void ApplicationIsQuitting()
    {
        networkThread.Abort();
        client.Close();
    }

    static void StartNetworkThread()
    {
        try
        {
            ThreadStart ts = new ThreadStart(ReadFromSocket_Thread);
            networkThread = new Thread(ts);
            networkThread.IsBackground = true;
            networkThread.Start();
        }
        catch (Exception ex)
        {
            Debug.Log("On client connect exception " + ex);
        }  
    }

    static void ReadFromSocket_Thread()
    {
        byte[] sizeBuffer;
        int packetSize = 0;
        while (true)
        {
            sizeBuffer = SocketReadAll(client, 4);
            packetSize = BitConverter.ToInt32(sizeBuffer, 0);
            byte[] packet = SocketReadAll(client, packetSize);
            PacketQueue.Enqueue(packet);
        }
    }

    static byte[] SocketReadAll(Socket s, int count)
    {
        int receivedCount = 0;
        byte[] ret = new byte[count];
        while (receivedCount < count)
        {
            receivedCount = client.Receive(ret, 0, count, SocketFlags.None);
        }
        return ret;
    }
}
