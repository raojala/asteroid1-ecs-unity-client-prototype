//using System;
//using System.Collections.Generic;

//namespace EDictionary
//{
//    /// <summary>
//    /// EDictionary is a wrapper around C# normal dictionary, but raises event when adding or removing items.
//    /// </summary>
//    public class EDictionary<T1, T2> : Dictionary<T1, T2>
//    {

//        public Queue<T1> CreateQueue = new Queue<T1>();
//        public Queue<T1> DeleteQueue = new Queue<T1>();
//        public new void Add(T1 key, T2 val)
//        {
//            try
//            {
//                base.Add(key, val);
//                CreateQueue.Enqueue(key);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }

//        public new void Remove(T1 key)
//        {
//            try
//            {
//                base.Remove(key);
//                DeleteQueue.Enqueue(key);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }
//    }
//}
