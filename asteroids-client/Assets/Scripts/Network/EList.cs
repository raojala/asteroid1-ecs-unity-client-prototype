using System;
using System.Collections.Generic;

namespace EList
{
    /// <summary>
    /// EDictionary is a wrapper around C# normal dictionary, but raises event when adding or removing items.
    /// </summary>
    public class EList<T1> : List<T1>
    {

        public Queue<T1> CreateQueue = new Queue<T1>();
        public new void Add(T1 item)
        {
            try
            {
                base.Add(item);
                CreateQueue.Enqueue(item);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}