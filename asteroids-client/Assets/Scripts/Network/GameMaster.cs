using System.Collections.Generic;
using UnityEngine;


public class GameMaster : MonoBehaviour
{

    private void LateUpdate()
    {
        if (SocketClient.PacketQueue.Count > 0)
        {
            byte[] packet = SocketClient.PacketQueue.Dequeue();
            NetworkTree.Ecs = Protocol.Ecs.Parser.ParseFrom(packet);

            foreach (uint key in NetworkTree.Ecs.Entities.Keys)
            {
                bool shouldAdd = true;

                foreach (uint keyString in GameMasterData.uuids)
                {
                    if (key == keyString)
                    {
                        shouldAdd = false;
                        break;
                    }
                }

                if (shouldAdd)
                {
                    GameMasterData.uuids.Add(key);
                }
            }
        }
    }
}
