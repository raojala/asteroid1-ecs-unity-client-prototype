using UnityEngine;

public class SocketReceiverGO : MonoBehaviour
{
    private void Update()
    {
        while (GameMasterData.uuids.CreateQueue.Count > 0)
        {
            uint uuid = GameMasterData.uuids.CreateQueue.Dequeue();
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.name = uuid.ToString();
            Asteroid a = go.AddComponent<Asteroid>();
            a.uuid = uuid;
        }
    }
}


