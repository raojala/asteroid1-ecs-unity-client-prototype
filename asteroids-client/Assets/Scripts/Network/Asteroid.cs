using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public uint uuid;
    Vector3 newPos = new Vector3();

    private void Update()
    {
        SetPosition();
    }

    void SetPosition()
    {
        newPos.x = NetworkTree.Ecs.Transforms[uuid].Location.X;
        newPos.y = NetworkTree.Ecs.Transforms[uuid].Location.Y;
        newPos.z = NetworkTree.Ecs.Transforms[uuid].Location.Z;
        gameObject.transform.position = newPos;
    }
}
